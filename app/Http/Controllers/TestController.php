<?php

namespace App\Http\Controllers;

use App\UserTest;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        $data = UserTest::get();
        return view('index')->with('data',$data);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['parity'] = 0;
        $store = UserTest::create($input);
        if($store->id % 2 == 0){
            $store->parity = 2;
            $store->save();
        }else{
            $store->parity = 1;
            $store->save();
        }
        return redirect()->route('index');
    }
}
