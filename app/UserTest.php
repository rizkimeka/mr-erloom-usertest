<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTest extends Model
{
    protected $table = 'user_tests';
    public $timestamps = false;

    protected $guarded = [];
}
