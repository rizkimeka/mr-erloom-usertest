--
-- File generated with SQLiteStudio v3.2.1 on Thu Oct 31 14:31:40 2019
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: migrations
CREATE TABLE "migrations" ("id" integer not null primary key autoincrement, "migration" varchar not null, "batch" integer not null);
INSERT INTO migrations (id, migration, batch) VALUES (5, '2019_09_02_035338_create_user_tests_table', 1);

-- Table: user_tests
CREATE TABLE "user_tests" ("id" integer not null primary key autoincrement, "name" varchar not null, "parity" varchar not null, "created_at" datetime null, "updated_at" datetime null);
INSERT INTO user_tests (id, name, parity, created_at, updated_at) VALUES (1, 'Muhamad Rizki', '1', NULL, NULL);
INSERT INTO user_tests (id, name, parity, created_at, updated_at) VALUES (2, 'Test', '2', NULL, NULL);
INSERT INTO user_tests (id, name, parity, created_at, updated_at) VALUES (3, 'Oke', '1', NULL, NULL);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
